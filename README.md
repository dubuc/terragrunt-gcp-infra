# Terragrunt GCP Infra

This repository will use terragrunt to create GCP infrastructure. The project will deploy a private GKE cluster, it's associated network and deploy Flux in the cluster while binding a GitHub repository to sync Kubernetes manifests.


## Required CI variables

| Name                           | Description                                                        |
|--------------------------------|--------------------------------------------------------------------|
| GITHUB_TOKEN                   | The GitHub access token to create the necessary Flux Deploy Key.   |
| GOOGLE_PROJECT                 | The GCP project where the Terraform remote backend bucket resides. |
| GOOGLE_REGION                  | The GCP region of the remote backend.                              |
| GCP_TERRAGRUNT_BUCKET          | Name of the GCP storage bucket to use.                             |
| GOOGLE_APPLICATION_CREDENTIALS | Path to the Service Principal credentials.                         |
