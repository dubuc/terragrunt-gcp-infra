# ------------------------------------------------------------------------
# Output full modules to be consumable from remote state file
# ------------------------------------------------------------------------

output "vpc" {
  value = module.gke-network
}

output "cluster" {
  value = module.gke
}