locals {
  region_vars  = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  project_vars = read_terragrunt_config(find_in_parent_folders("project.hcl"))

  # Extract values
  region  = local.region_vars.locals.region
  project = local.project_vars.locals.project_name
}

include {
  path = find_in_parent_folders()
}

inputs = {
  cluster_name      = "gke-prod-us-central1"
  region            = local.region
  project           = local.project
  github_repository = "https://github.com/dubuc/ambassador-flux"
}