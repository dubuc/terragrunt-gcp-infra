# ------------------------------------------------------------------------
# Required Variables
# ------------------------------------------------------------------------

variable "cluster_name" {
  type = string
  description = "The Kubernetes cluster name."
}

variable "project" {
  type = string
  description = "The project ID for our infrastructure deployment."
}

variable "region" {
  type = string
  description = "The deployment target region."
}

variable "github_repository" {
  type = string
  description = "The URL of the GitHub repository where the Kubernetes manifests reside."
}

# ------------------------------------------------------------------------
# Optional Variables
# ------------------------------------------------------------------------

variable "flux_namespace" {
  type = string
  description = "The namespace name where to deploy Flux"
  default = "fluxcd"
}