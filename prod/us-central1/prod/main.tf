# This terraform code will deploy FluxCD on a GKE cluster and deploy an Ambassador API Gateway

terraform {
  required_version = ">= 0.12.7"
}

locals {
  github_url_sanitized = trimprefix(var.github_repository, "https://")
  github_repository_path = trimprefix(local.github_url_sanitized, "github.com/")
}

# Workaround
data "template_file" "gke_host_endpoint" {
  template = module.gke.endpoint
}

data "template_file" "access_token" {
  template = data.google_client_config.client.access_token
}

data "template_file" "cluster_ca_certificate" {
  template = module.gke.ca_certificate
}

# ------------------------------------------------------------------------
# Project Providers
# ------------------------------------------------------------------------

provider "google" {
  version = "3.16.0"
}

provider "google-beta" {
  version = "3.23.0"
}

# Workaround from Gruntworks to authenticate the Kubernetes provider
data "google_client_config" "client" {}

provider "kubernetes" {
  version = "1.11.3"

  load_config_file = false
  host = data.template_file.gke_host_endpoint.rendered
  cluster_ca_certificate = data.template_file.cluster_ca_certificate.rendered
  token = data.template_file.access_token.rendered
}

provider "helm" {
  version = "= 1.2.1"

  kubernetes {
    load_config_file = false
    host = data.template_file.gke_host_endpoint.rendered
    cluster_ca_certificate = data.template_file.cluster_ca_certificate.rendered
    token = data.template_file.access_token.rendered
  }
}

provider "github" {
  version = "= 2.8.0"
  individual = true
}

# ------------------------------------------------------------------------
# Network
# ------------------------------------------------------------------------

module "gke-network" {
  source       = "github.com/terraform-google-modules/terraform-google-network?ref=v2.3.0"

  project_id   = var.project
  network_name = "${var.cluster_name}-network"

  subnets = [
    {
      subnet_name   = "${var.cluster_name}-subnet"
      subnet_ip     = "10.0.0.0/24"
      subnet_region = var.region
    },
  ]

  secondary_ranges = {
    "${var.cluster_name}-subnet" = [
      {
        range_name    = "${var.cluster_name}-pods"
        ip_cidr_range = "10.1.0.0/16"
      },
      {
        range_name    = "${var.cluster_name}-services"
        ip_cidr_range = "10.2.0.0/20"
      },
    ] }
}

# ------------------------------------------------------------------------
# Deploy the GKE cluster
# ------------------------------------------------------------------------

module "gke" {
  source                            = "github.com/terraform-google-modules/terraform-google-kubernetes-engine//modules/private-cluster-update-variant?ref=v9.2.0"
  project_id                        = var.project
  name                              = var.cluster_name
  region                            = var.region
  regional                          = true
  network                           = module.gke-network.network_name
  subnetwork                        = module.gke-network.subnets_names[0]
  ip_range_pods                     = module.gke-network.subnets_secondary_ranges[0].*.range_name[0]
  ip_range_services                 = module.gke-network.subnets_secondary_ranges[0].*.range_name[1]
  enable_private_endpoint           = true
  enable_private_nodes              = true
  master_ipv4_cidr_block            = "172.16.0.16/28"
  network_policy                    = true
  horizontal_pod_autoscaling        = true
  service_account                   = "create"
  remove_default_node_pool          = true
  disable_legacy_metadata_endpoints = true

  master_authorized_networks = [
    {
      cidr_block   = module.gke-network.subnets_ips[0]
      display_name = "VPC"
    },
  ]

  node_pools = [
    {
      name               = "generic"
      machine_type       = "n1-standard-2"
      min_count          = 1
      max_count          = 100
      disk_size_gb       = 100
      disk_type          = "pd-ssd"
      image_type         = "COS"
      auto_scaling       = true
      auto_repair        = true
      auto_upgrade       = false
      preemptible        = false
      initial_node_count = 1
    },
  ]

  node_pools_oauth_scopes = {
    all = [
      "https://www.googleapis.com/auth/trace.append",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/servicecontrol",
    ]

    my-node-pool = [
      "https://www.googleapis.com/auth/trace.append",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/servicecontrol",
    ]
  }

  node_pools_labels = {

    all = {}
    my-node-pool = {}
  }

  node_pools_metadata = {
    all = {}

    my-node-pool = {}

  }

  node_pools_tags = {
    all = []

    my-node-pool = []

  }
}

# ------------------------------------------------------------------------
# Deploy FluxCD pointing to our Kubernetes manifests
# ------------------------------------------------------------------------

resource "tls_private_key" "git_deploy_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "kubernetes_namespace" "fluxcd" {
  metadata {
    name = var.flux_namespace
  }
}

# We need to create the SSH Private Key and store it as a Kubernetes secret. If we don't do this, the fluxcd deployment
# will generate a random SSH key for us.
resource "kubernetes_secret" "k8s_git_deploy" {
  metadata {
    name      = "k8s-git-deploy"
    namespace = kubernetes_namespace.fluxcd.metadata[0].name
  }
  type = "Opaque"
  data = {
    identity = tls_private_key.git_deploy_key.private_key_pem
  }
}

module "flux" {

  source = "../../../modules/tf-helm-flux"

  release_name = "flux"
  namespace = var.flux_namespace

  git_repo_url = local.github_url_sanitized
  git_path = "deploy"
  git_secret_name = kubernetes_secret.k8s_git_deploy.metadata[0].name

}

# ------------------------------------------------------------------------
# Configure GitHub Deploy Key
# ------------------------------------------------------------------------

resource "github_repository_deploy_key" "flux" {
  title      = "fluxcd"
  repository = local.github_repository_path
  key        = chomp(tls_private_key.git_deploy_key.public_key_openssh)
  # Enable read only for Flux
  read_only = "true"
}

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# We have configured Flux to sync the Ambassador git repository, it will be picked up and deployed. We can
# use Ambassador to perform Canary releases for our services.
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
