remote_state {
  backend = "gcs"
  config = {
    project              = "${get_env("GOOGLE_PROJECT")}"
    location             = "${get_env("GOOGLE_REGION", "us-central")}"
    bucket               = "${get_env("GCP_TERRAGRUNT_BUCKET")}"
    skip_bucket_creation = true
    prefix               = "${path_relative_to_include()}/terraform.tfstate"
    credentials          = "${get_env("GOOGLE_APPLICATION_CREDENTIALS")}"
  }
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite_terragrunt"
  }
}