# ------------------------------------------------------------
# Required Values
# ------------------------------------------------------------

variable "git_repo_url" {
  type        = string
  description = "The URL of the git repository with the Kubernetes manifests to deploy."
}

variable "git_secret_name" {
  type        = string
  description = "The Kubernetes Secret containing the SSH Private Key to be used by fluxd."
}

# ------------------------------------------------------------
# Optional Values
# ------------------------------------------------------------
variable "release_name" {
  type        = string
  description = "The release name."
  default     = "fluxcd"
}

variable "namespace" {
  type        = string
  description = "The namespace to deploy fluxd. Defaults to fluxcd"
  default     = "fluxcd"
}

variable "chart_name" {
  type        = string
  description = "The Flux chart name."
  default     = "flux"
}

variable "chart_version" {
  type = string
  description = "The Helm Chart version for flux"
  default = "1.3.0"
}

variable "chart_repository" {
  type        = string
  description = "Repository from where to fetch the Flux chart."
  default     = "https://charts.fluxcd.io"
}

variable "git_readonly" {
  type        = bool
  description = "Enable readonly support from fluxd. Flux will no longer try to write to the flux-sync branch of your repository"
  default     = true
}

variable "image_repository" {
  type        = string
  description = "The image repository to where to pull the Flux container image from."
  default     = "docker.io/fluxcd/flux"
}

variable "image_tag" {
  type        = string
  description = "The image tag for Flux."
  default     = "1.19.0"
}

variable "git_poll_interval" {
  type        = string
  description = "The interval between git polls. Default: 5m"
  default     = "5m"
}

variable "git_path" {
  type        = string
  description = "The directory where the Kubernetes manifests are located."
  default     = "/"
}

variable "enable_sync_garbage_collection" {
  type        = bool
  description = "Flux will remove resources that it created, but are no longer present in git."
  default     = true
}

variable "enable_manifest_generation" {
  type        = bool
  description = "Manifest generation is required when using Kustomize or another manifest generator. This relies on a .flux.yaml present in the repo."
  default     = true
}