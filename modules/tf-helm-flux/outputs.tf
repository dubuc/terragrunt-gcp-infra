# We expose the values as module output so it can be used by other resources or terraform_remote_state data source
output "values" {
  value = helm_release.flux.values
}