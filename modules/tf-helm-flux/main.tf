# ---------------------------------------------------------------------------------------------------
# DEPLOY FLUX IN READ ONLY MODE
# This module will deploy a fluxcd instance in read-only mode by default on your cluster. It has
# manifest generation and garbage collection enabled by default.
# ---------------------------------------------------------------------------------------------------
terraform {
  required_version = ">= 0.12.7"
}

resource "helm_release" "flux" {
  chart            = var.chart_name
  version          = var.chart_version
  repository       = var.chart_repository
  name             = var.release_name
  namespace        = var.namespace
  create_namespace = true

  set {
    name  = "image.repository"
    value = var.image_repository
  }

  # If no image tag is defined, use the chart default
  set {
    name  = "image.tag"
    value = var.image_tag
  }

  set {
    name  = "git.url"
    value = var.git_repo_url
  }

  set {
    name  = "git.pollInterval"
    value = var.git_poll_interval
  }

  set {
    name  = "git.secretName"
    value = var.git_secret_name
  }

  set {
    name  = "git.readonly"
    value = var.git_readonly
  }

  set {
    name  = "syncGarbageCollection.enabled"
    value = var.enable_sync_garbage_collection
  }

  set {
    name  = "manifestGeneration"
    value = var.enable_manifest_generation
  }

}